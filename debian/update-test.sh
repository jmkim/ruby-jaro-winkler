#!/bin/sh
#
# This script will update the debian/test/* files from upstream.
# This script should run in debian/ directory.
#
# Copyright © 2019 Jongmin Kim <jmkim@pukyong.ac.kr>
#
# MIT License (Expat)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# .
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# .
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

if [ ! -d "../debian/" ]; then
  >&2 echo "Run this script in debian/ directory!"
  exit 1
fi

# Check script dependencies
command -v wget > /dev/null || ! echo "wget not available." || exit 2
command -v unzip > /dev/null || ! echo "unzip not available." || exit 3

version="$(git tag | sed -nr 's/^upstream\/([0-9\.])/\1/p')"
upstreamurl="https://github.com/tonytonyjan/jaro_winkler/archive/v${version}.zip"
outdir="test"
tmpoutfile="$(mktemp -u)" # Temp upstream download file path
tmpoutdir="$(mktemp -d)"  # Temp unzip directory

echo -n "Updating test files to '${version}'... "

# Download from upstream and unzip
wget "${upstreamurl}" -O "${tmpoutfile}" -o /dev/null || ! echo "ERROR: Download failed! (url: ${upstreamurl})" || exit 4
unzip "${tmpoutfile}" -d "${tmpoutdir}/" > /dev/null || ! echo "ERROR: Unzip failed!" || exit 5

# Replace old test files to new
rm -rf "${outdir}/"*
mv "${tmpoutdir}/jaro_winkler-${version}/test/"* "${outdir}/"

# Clean downloaded file and unzipped dir
rm -f "${tmpoutfile}"
rm -rf "${tmpoutdir}/"

echo "Done!"
